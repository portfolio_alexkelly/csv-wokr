def posterior(prior, likelihood, observation):
    p_obs_given_true = 1.0
    for i, obs in enumerate(observation):
        if obs:
            p_obs_given_true *= likelihood[i][True]
        else:
            p_obs_given_true *= (1 - likelihood[i][True])
    
    p_obs_given_false = 1.0
    for i, obs in enumerate(observation):
        if obs:
            p_obs_given_false *= likelihood[i][False]
        else:
            p_obs_given_false *= (1 - likelihood[i][False])
    
    p_obs = p_obs_given_true * prior + p_obs_given_false * (1 - prior)
    p_true_given_obs = (p_obs_given_true * prior) / p_obs
    
    return p_true_given_obs



import csv

def learn_prior(file_name, pseudo_count=0):
    with open(file_name) as in_file:
        training_examples = [tuple(row) for row in csv.reader(in_file)]

    # Remove the header
    training_examples = training_examples[1:]

    # Count the number of spam examples
    spam_count = sum(1 for example in training_examples if example[-1] == '1')
    
    # Calculate the prior probability with pseudo-counts
    prior = (spam_count + pseudo_count) / (len(training_examples) + 2 * pseudo_count)

    return prior


import csv

def learn_likelihood(file_name, pseudo_count=0):
    with open(file_name) as in_file:
        training_examples = [tuple(row) for row in csv.reader(in_file)]

    # Remove the header
    training_examples = training_examples[1:]

    # Initialize counts
    feature_given_spam_counts = [0] * 12
    feature_given_not_spam_counts = [0] * 12
    spam_count = 0
    not_spam_count = 0

    # Count the occurrences
    for example in training_examples:
        is_spam = example[-1] == '1'
        if is_spam:
            spam_count += 1
        else:
            not_spam_count += 1

        for i in range(12):
            if example[i] == '1':
                if is_spam:
                    feature_given_spam_counts[i] += 1
                else:
                    feature_given_not_spam_counts[i] += 1

    # Calculate likelihoods
    likelihoods = []
    for i in range(12):
        p_feature_given_spam = (feature_given_spam_counts[i] + pseudo_count) / (spam_count + 2 * pseudo_count)
        p_feature_given_not_spam = (feature_given_not_spam_counts[i] + pseudo_count) / (not_spam_count + 2 * pseudo_count)
        
        likelihoods.append((p_feature_given_not_spam, p_feature_given_spam))

    return likelihoods


import csv


def learn_prior(file_name, pseudo_count=0):
    with open(file_name) as in_file:
        training_examples = [tuple(row) for row in csv.reader(in_file)]
    training_examples = training_examples[1:]
    spam_count = sum(1 for example in training_examples if example[-1] == '1')
    prior = (spam_count + pseudo_count) / (len(training_examples) + 2 * pseudo_count)
    return prior

def learn_likelihood(file_name, pseudo_count=0):
    with open(file_name) as in_file:
        training_examples = [tuple(row) for row in csv.reader(in_file)]
    training_examples = training_examples[1:]
    feature_given_spam_counts = [0] * 12
    feature_given_not_spam_counts = [0] * 12
    spam_count = 0
    not_spam_count = 0
    for example in training_examples:
        is_spam = example[-1] == '1'
        if is_spam:
            spam_count += 1
        else:
            not_spam_count += 1
        for i in range(12):
            if example[i] == '1':
                if is_spam:
                    feature_given_spam_counts[i] += 1
                else:
                    feature_given_not_spam_counts[i] += 1
    likelihoods = []
    for i in range(12):
        p_feature_given_spam = (feature_given_spam_counts[i] + pseudo_count) / (spam_count + 2 * pseudo_count)
        p_feature_given_not_spam = (feature_given_not_spam_counts[i] + pseudo_count) / (not_spam_count + 2 * pseudo_count)
        likelihoods.append((p_feature_given_not_spam, p_feature_given_spam))
    return likelihoods


def nb_classify(prior, likelihood, input_vector):
    p_spam_given_input = prior
    p_not_spam_given_input = 1 - prior

    for i, feature in enumerate(input_vector):
        if feature:
            p_spam_given_input *= likelihood[i][True]
            p_not_spam_given_input *= likelihood[i][False]
        else:
            p_spam_given_input *= (1 - likelihood[i][True])
            p_not_spam_given_input *= (1 - likelihood[i][False])

    
    normalization_factor = p_spam_given_input + p_not_spam_given_input
    p_spam_given_input /= normalization_factor
    p_not_spam_given_input /= normalization_factor

    if p_spam_given_input > p_not_spam_given_input:
        return ("Spam", p_spam_given_input)
    else:
        return ("Not Spam", p_not_spam_given_input)
